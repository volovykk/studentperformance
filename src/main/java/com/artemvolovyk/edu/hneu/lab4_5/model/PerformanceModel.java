package com.artemvolovyk.edu.hneu.lab4_5.model;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.time.LocalDateTime;

public class PerformanceModel {

    private Integer performanceId;
    private LocalDateTime date;
    private String subjectName;
    private String lastName;
    private String firstName;
    private String middleName;
    private String groupNo;
    private String course;
    private String attendedLectures;
    private String labWorksCompleted;
    private String labWorksGrades;

    public PerformanceModel() {

    }

    public PerformanceModel(
            String subjectName, String lastName, String firstName,
            String middleName, String groupNo, String course,
            String attendedLectures, String labWorksCompleted, String labWorksGrades) {
        this.subjectName = subjectName;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.groupNo = groupNo;
        this.course = course;
        this.attendedLectures = attendedLectures;
        this.labWorksCompleted = labWorksCompleted;
        this.labWorksGrades = labWorksGrades;
    }

    public Integer getId() {
        return performanceId;
    }

    public Integer getPerformanceId() {
        return performanceId;
    }

    public void setPerformanceId(Integer performanceId) {
        this.performanceId = performanceId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getAttendedLectures() {
        return attendedLectures;
    }

    public void setAttendedLectures(String attendedLectures) {
        this.attendedLectures = attendedLectures;
    }

    public String getLabWorksCompleted() {
        return labWorksCompleted;
    }

    public void setLabWorksCompleted(String labWorksCompleted) {
        this.labWorksCompleted = labWorksCompleted;
    }

    public String getLabWorksGrades() {
        return labWorksGrades;
    }

    public void setLabWorksGrades(String labWorksGrades) {
        this.labWorksGrades = labWorksGrades;
    }

    @Override
    public String toString() {
        return String.format("com.artemvolovyk.edu.hneu.kpp.lab4_5.model.Performance " +
                        "[performance_id=%s, " +
                        "date=%s, " +
                        "subject_name=%s, " +
                        "last_name=%s, " +
                        "first_name=%s, " +
                        "middle_name=%s, " +
                        "group_no=%s, " +
                        "course=%s, " +
                        "attended_lectures=%s, " +
                        "lab_works_completed=%s, " +
                        "lab_works_grades=%s]",
                performanceId, date, subjectName, lastName, firstName, middleName,
                groupNo, course, attendedLectures, labWorksCompleted, labWorksGrades);
    }
}
