package com.artemvolovyk.edu.hneu.lab4_5.controller;

import com.artemvolovyk.edu.hneu.lab4_5.util.Converter;
import com.artemvolovyk.edu.hneu.lab4_5.model.Performance;
import com.artemvolovyk.edu.hneu.lab4_5.model.PerformanceModel;
import com.artemvolovyk.edu.hneu.lab4_5.service.PerformanceService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = "/PerformanceController")
public class PerformanceController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static final String INSERT_OR_EDIT = "/jsp/addPerformanceForm.jsp";
    private static final String LIST_PERFORMANCE = "/jsp/studentPerformanceTable.jsp";
    private final PerformanceService performanceService;

    public PerformanceController() {
        super();
        performanceService = new PerformanceService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward;
        String action = request.getParameter("action");
        List<Performance> performanceList = new ArrayList<>();

        if (action.equalsIgnoreCase("delete")) {
            int performanceId = Integer.parseInt(request.getParameter("performance_id"));
            performanceService.delete(performanceId);
            forward = LIST_PERFORMANCE;
            performanceList = performanceService.findAll();
            request.setAttribute("performances", performanceList);
            request.setAttribute("formatted_dates", formatDates(performanceList));
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int performanceId = Integer.parseInt(request.getParameter("performance_id"));
            Performance performance = performanceService.findById(performanceId);
            request.setAttribute("performance", performance);
        } else if (action.equalsIgnoreCase("showAll")) {
            forward = LIST_PERFORMANCE;
            performanceList = performanceService.findAll();
            request.setAttribute("performances", performanceList);
            request.setAttribute("formatted_dates", formatDates(performanceList));
        } else {
            forward = INSERT_OR_EDIT;
            request.setAttribute("performances", performanceList);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PerformanceModel performanceModel = new PerformanceModel();
        boolean foundInputError = false;
        Map<String, String> userInputErrors = new HashMap<>();

        String subjectNameParam = request.getParameter("subjectName");
        String lastNameParam = request.getParameter("lastName");
        String firstNameParam = request.getParameter("firstName");
        String middleNameParam = request.getParameter("middleName");
        String groupNoParam = request.getParameter("groupNo");
        String courseParam = request.getParameter("course");
        String attendedLecturesParam = request.getParameter("attendedLectures");
        String labWorksCompletedParam = request.getParameter("labWorksCompleted");
        String labWorksGradesParam = request.getParameter("labWorksGrades");

        performanceModel.setDate(LocalDateTime.now());
        performanceModel.setSubjectName(subjectNameParam);
        performanceModel.setLastName(lastNameParam);
        performanceModel.setFirstName(firstNameParam);
        performanceModel.setMiddleName(middleNameParam);
        performanceModel.setGroupNo(groupNoParam);
        performanceModel.setCourse(courseParam);
        performanceModel.setAttendedLectures(attendedLecturesParam);
        performanceModel.setLabWorksCompleted(labWorksCompletedParam);
        performanceModel.setLabWorksGrades(labWorksGradesParam);

        if(
                (subjectNameParam == null || subjectNameParam.isEmpty())
                || (lastNameParam == null || lastNameParam.isEmpty())
                || (firstNameParam == null || firstNameParam.isEmpty())
                || (groupNoParam == null || groupNoParam.isEmpty())
                || (courseParam == null || courseParam.isEmpty())
                || (attendedLecturesParam == null || attendedLecturesParam.isEmpty())
                || (labWorksCompletedParam == null || labWorksCompletedParam.isEmpty())
        ) {
            userInputErrors.put("emptyFieldsError", "Будь-ласка, заповніть усі обов'язкові поля");
            request.setAttribute("performance", performanceModel);
            request.setAttribute("userInputErrors", userInputErrors);
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        if (subjectNameParam.length() > 100) {
            userInputErrors.put("subjectNameError", "Назва предмету не має перевищувати 100 символів");
            foundInputError = true;
        }

        if (!firstNameParam.matches("^([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+){1,50}$")) {
            userInputErrors.put("firstNameError", "Некоректний формат для імені");
            foundInputError = true;
        }

        if (!lastNameParam.matches("(^([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+)(-[А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+)*){1,50}$")) {
            userInputErrors.put("lastNameError", "Некоректний формат для прізвища");
            foundInputError = true;
        }

        if (middleNameParam != null
            && !middleNameParam.isEmpty()
            && !middleNameParam.matches("^([А-ЩЬЮЯҐЄІЇ][а-щьюяґєії]+){1,50}$"))
        {
            userInputErrors.put("middleNameError", "Некоректний формат для по-батькові");
            foundInputError = true;
        }

        if (!groupNoParam.matches("^([0-9]+(\\.[0-9]+)*){1,100}$")) {
            userInputErrors.put("groupNoError", "Некоректний формат для номеру групи");
            foundInputError = true;
        }

        if (!courseParam.matches("^[1-9]$")) {
            userInputErrors.put("courseError", "Некоректний формат курсу");
            foundInputError = true;
        }

        try {
            int attendedLecturesParamParsed = Integer.parseInt(attendedLecturesParam);
            if (attendedLecturesParamParsed < 0) {
                userInputErrors.put("attendedLecturesError", "Кількість відвіданих лекцій не може бути менше нуля");
            }
        } catch (NumberFormatException e) {
            userInputErrors.put("attendedLecturesError", "Некоректний формат кількості відвіданих лекцій");
            foundInputError = true;
        }

        try {
            int labWorksCompletedParamParsed = Integer.parseInt(labWorksCompletedParam);
            if (labWorksCompletedParamParsed < 0) {
                userInputErrors.put("labWorksCompletedError", "Кількість виконаних лабораторних не може бути менше нуля");
            }
        } catch (NumberFormatException e) {
            userInputErrors.put("labWorksCompletedError", "Некоректний формат кількості виконаних лаб. робіт");
            foundInputError = true;
        }

        if (!labWorksGradesParam.matches("^(ЛР[1-9]([0-9]+)?: [1-9]([0-9]+)?(, ЛР[1-9]([0-9]+)?: [1-9]([0-9]+)?)*){0,255}$")) {
            userInputErrors.put("labWorksGradesError", "Некоректний формат оцінок за лаб. роботи: ЛР<номер>: <оцінка>, ...");
            foundInputError = true;
        }
        else {
            String formatted_for_table_grades = labWorksGradesParam.replaceAll(", ", "<br>");
            performanceModel.setLabWorksGrades(formatted_for_table_grades);
        }

        String performanceId = request.getParameter("performanceId");
        if (foundInputError) {
            request.setAttribute("performance", performanceModel);
            request.setAttribute("userInputErrors", userInputErrors);

            String action = request.getParameter("action");
            if (action != null && !action.isEmpty()) {
                request.setAttribute("action", action);
            }
            if (performanceId != null && !performanceId.isEmpty()) {
                performanceModel.setPerformanceId(Integer.parseInt(performanceId));
                request.setAttribute("performance_id", performanceId);
            }

            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        if (performanceId == null || performanceId.isEmpty()) {
            Performance performanceEntity = new Performance();
            Converter.PerformanceModelToPerformanceEntity(performanceEntity, performanceModel);
            performanceService.persist(performanceEntity); // Сохраняем новую сущность в базе данных
        } else {
            performanceModel.setPerformanceId(Integer.parseInt(performanceId));
            Performance performanceEntity = performanceService.findById(performanceModel.getId()); // Получаем существующую сущность из базы данных
            if (performanceEntity == null) {
                // Обработка случая, когда сущность не найдена в базе данных
                request.setAttribute("errorMessage", "Редагований запис не знайдено");
                RequestDispatcher view = request.getRequestDispatcher(LIST_PERFORMANCE);
                view.forward(request, response);
                return;
            }
            Converter.PerformanceModelToPerformanceEntity(performanceEntity, performanceModel);
            performanceService.update(performanceEntity); // Обновляем существующую сущность в базе данных
        }

        response.sendRedirect(request.getContextPath() + "/PerformanceController?action=showAll");
    }

    String[] formatDates(List<Performance> performanceList) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        String[] formattedDates = new String[performanceList.size()];
        for (int i = 0; i < performanceList.size(); i++) {
            String formattedDate = performanceList.get(i).getDate().format(formatter);
            formattedDates[i] = formattedDate;
        }

        return formattedDates;
    }
}

