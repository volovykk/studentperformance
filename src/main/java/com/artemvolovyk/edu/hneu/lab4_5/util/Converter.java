package com.artemvolovyk.edu.hneu.lab4_5.util;

import com.artemvolovyk.edu.hneu.lab4_5.model.Performance;
import com.artemvolovyk.edu.hneu.lab4_5.model.PerformanceModel;

public class Converter {
    public static void PerformanceModelToPerformanceEntity(Performance performance, PerformanceModel model ) {
        performance.setPerformanceId(model.getPerformanceId());
        performance.setDate(model.getDate());
        performance.setSubjectName(model.getSubjectName());
        performance.setLastName(model.getLastName());
        performance.setFirstName(model.getFirstName());
        performance.setMiddleName(model.getMiddleName());
        performance.setGroupNo(model.getGroupNo());
        performance.setCourse(Integer.parseInt(model.getCourse()));
        performance.setAttendedLectures(Integer.parseInt(model.getAttendedLectures()));
        performance.setLabWorksCompleted(Integer.parseInt(model.getLabWorksCompleted()));
        performance.setMiddleName(model.getMiddleName());
        performance.setLabWorksGrades(model.getLabWorksGrades());
    }

//    public static void PerformanceEntityToPerformanceModel(PerformanceModel model, Performance performance) {
//        model.setPerformanceId(performance.getPerformanceId());
//        model.setDate(performance.getDate());
//        model.setSubjectName(performance.getSubjectName());
//        model.setLastName(performance.getLastName());
//        model.setFirstName(performance.getFirstName());
//        model.setMiddleName(performance.getMiddleName());
//        model.setGroupNo(performance.getGroupNo());
//        model.setCourse(performance.getCourse());
//        model.setAttendedLectures(performance.getAttendedLectures());
//        model.setLabWorksCompleted(performance.getLabWorksCompleted());
//        model.setMiddleName(performance.getMiddleName());
//        model.setLabWorksGrades(performance.getLabWorksGrades());
//    }
}
