package com.artemvolovyk.edu.hneu.lab4_5.model;

import jakarta.persistence.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Entity
@Table (name = "performance")
public class Performance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "performance_id")
    private Integer performanceId;

    @Column(name = "date", nullable = false)
    @CreationTimestamp
    private LocalDateTime date;

    @Column(name = "subject_name", nullable = false)
    private String subjectName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "middle_name", nullable = false)
    private String middleName;

    @Column(name = "group_no", nullable = false)
    private String groupNo;

    @Column(name = "course", nullable = false)
    private Integer course;

    @Column(name = "attended_lectures", columnDefinition = "INT DEFAULT '0'")
    private Integer attendedLectures;

    @Column(name = "lab_works_completed", columnDefinition = "INT DEFAULT '0'")
    private Integer labWorksCompleted;

    @Column(name = "lab_works_grades")
    private String labWorksGrades;

    public Performance() {

    }

    public Performance(String subjectName,
                       String lastName, String firstName, String middleName,
                       String groupNo, Integer course, Integer attendedLectures,
                       Integer labWorksCompleted, String labWorksGrades) {
        this.subjectName = subjectName;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.groupNo = groupNo;
        this.course = course;
        this.attendedLectures = attendedLectures;
        this.labWorksCompleted = labWorksCompleted;
        this.labWorksGrades = labWorksGrades;
    }

    public Integer getId() {
        return performanceId;
    }

    public Integer getPerformanceId() {
        return performanceId;
    }

    public void setPerformanceId(Integer performanceId) {
        this.performanceId = performanceId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getGroupNo() {
        return groupNo;
    }

    public void setGroupNo(String groupNo) {
        this.groupNo = groupNo;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public Integer getAttendedLectures() {
        return attendedLectures;
    }

    public void setAttendedLectures(Integer attendedLectures) {
        this.attendedLectures = attendedLectures;
    }

    public Integer getLabWorksCompleted() {
        return labWorksCompleted;
    }

    public void setLabWorksCompleted(Integer labWorksCompleted) {
        this.labWorksCompleted = labWorksCompleted;
    }

    public String getLabWorksGrades() {
        return labWorksGrades;
    }

    public void setLabWorksGrades(String labWorksGrades) {
        this.labWorksGrades = labWorksGrades;
    }

    @Override
    public String toString() {
        return String.format("com.artemvolovyk.edu.hneu.kpp.lab4_5.model.Performance " +
                        "[performance_id=%s, " +
                        "date=%s, " +
                        "subject_name=%s, " +
                        "last_name=%s, " +
                        "first_name=%s, " +
                        "middle_name=%s, " +
                        "group_no=%s, " +
                        "course=%s, " +
                        "attended_lectures=%s, " +
                        "lab_works_completed=%s, " +
                        "lab_works_grades=%s]",
                performanceId, date, subjectName, lastName, firstName, middleName,
                groupNo, course, attendedLectures, labWorksCompleted, labWorksGrades);
    }
}
