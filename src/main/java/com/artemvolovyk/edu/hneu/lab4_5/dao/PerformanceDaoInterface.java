package com.artemvolovyk.edu.hneu.lab4_5.dao;

import java.io.Serializable;
import java.util.List;

public interface PerformanceDaoInterface<T, Id extends Serializable> {
    public void persist(T entity);
    public void update(T entity);
    public T findById(Id id);
    public void delete(T entity);
    public List<T> findAll();
    void deleteAll();
}