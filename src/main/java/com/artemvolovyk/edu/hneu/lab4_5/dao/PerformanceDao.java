package com.artemvolovyk.edu.hneu.lab4_5.dao;

import com.artemvolovyk.edu.hneu.lab4_5.model.Performance;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class PerformanceDao implements PerformanceDaoInterface<Performance, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    private static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        return configuration.buildSessionFactory();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void persist(Performance entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Performance entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Performance findById(Integer id) {
        return getCurrentSession().get(Performance.class, id);
    }

    @Override
    public void delete(Performance entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Performance> findAll() {
        return (List<Performance>) getCurrentSession().createQuery("from Performance", Performance.class).getResultList();
    }

    @Override
    public void deleteAll() {
        List<Performance> entityList = findAll();
        for(Performance entity : entityList) {
            delete(entity);
        }
    }
}



