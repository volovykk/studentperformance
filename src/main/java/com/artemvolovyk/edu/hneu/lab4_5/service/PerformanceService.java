package com.artemvolovyk.edu.hneu.lab4_5.service;

import com.artemvolovyk.edu.hneu.lab4_5.dao.PerformanceDao;
import com.artemvolovyk.edu.hneu.lab4_5.model.Performance;

import java.util.List;

public class PerformanceService {

    private static PerformanceDao performanceDao;
    public PerformanceService () {
        performanceDao = new PerformanceDao();
    }

    public void persist (Performance entity) {
        performanceDao.openCurrentSessionWithTransaction();
        performanceDao.persist(entity);
        performanceDao.closeCurrentSessionWithTransaction();
    }

    public void update (Performance entity) {
        performanceDao.openCurrentSessionWithTransaction();
        performanceDao.update(entity);
        performanceDao.closeCurrentSessionWithTransaction();
    }

    public Performance findById(Integer id) {
        performanceDao.openCurrentSession();
        Performance performance = performanceDao.findById(id);
        performanceDao.closeCurrentSession();
        return performance;
    }

    public void delete(Integer id) {
        performanceDao.openCurrentSessionWithTransaction();
        Performance performance = performanceDao.findById(id);
        performanceDao.delete(performance);
        performanceDao.closeCurrentSessionWithTransaction();
    }

    public List<Performance> findAll() {
        performanceDao.openCurrentSessionWithTransaction();
        List<Performance> performances = performanceDao.findAll();
        performanceDao.closeCurrentSessionWithTransaction();
        return performances;
    }

    public void deleteAll() {
        performanceDao.openCurrentSessionWithTransaction();
        performanceDao.deleteAll();
        performanceDao.closeCurrentSessionWithTransaction();
    }

    public PerformanceDao studentDao() {
        return performanceDao;
    }
}

