<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Успішність студентів</title>
    <link rel="stylesheet" href="../static/css/styles.css">
</head>
<body>
<div class="background"></div>
<div class="container">
    <h1>Успішність та відвідування занять студентами</h1>

    <div class=table_buttons>
        <a href="${pageContext.request.contextPath}/" class="btn">Головна</a>
        <a href="PerformanceController?action=insert" class="btn">Додати успішність студента за предметом</a>
    </div>

    <table class="table">
        <tr>
            <th>ID</th>
            <th>Дата</th>
            <th>Предмет</th>
            <th>Прізвище</th>
            <th>Ім'я</th>
            <th>По-батькові</th>
            <th>Номер групи</th>
            <th>Курс</th>
            <th>Відвідані лекції</th>
            <th>Виконані лаб. роботи</th>
            <th>Оцінки</th>
            <th>Дії</th>
        </tr>
        <c:forEach var="performance" items="${performances}" varStatus="loop">
        <tr>
            <td>${performance.getId()}</td>
            <td>${formatted_dates[loop.index]}</td>
            <td>${performance.getSubjectName()}</td>
            <td>${performance.getLastName()}</td>
            <td>${performance.getFirstName()}</td>
            <td>${performance.getMiddleName()}</td>
            <td>${performance.getGroupNo()}</td>
            <td>${performance.getCourse()}</td>
            <td>${performance.getAttendedLectures()}</td>
            <td>${performance.getLabWorksCompleted()}</td>
            <td>${performance.getLabWorksGrades()}</td>
            <td>
                <a href="PerformanceController?action=edit&performance_id=${performance.getId()}">Редагувати</a>
                <a href="PerformanceController?action=delete&performance_id=${performance.getId()}">Видалити</a>
            </td>
        </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>