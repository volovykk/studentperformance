<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Головна сторінка</title>
    <link rel="stylesheet" href="../static/css/styles.css">
</head>
<body>
<div class="container">

    <img src="../static/images/kfis_logo.png" alt="лого" class="logo">
    <h1>Облік успішності студентів</h1>
    <a href="PerformanceController?action=showAll" class="btn">Переглянути звіт успішності студентів</a>
</div>
</body>
</html>