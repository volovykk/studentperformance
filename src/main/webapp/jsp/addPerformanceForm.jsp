<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
    HashMap userInputErrors = (HashMap) request.getAttribute("userInputErrors");

    String actionURL = "PerformanceController";
    String performanceId = request.getParameter("performance_id");

    // Check if performance_id is empty
    if (performanceId == null || performanceId.isEmpty()) {
        // If performance_id is empty, construct action URL without performance_id parameter
        actionURL += "?action=" + request.getParameter("action");
    } else {
        // If performance_id is not empty, construct action URL with performance_id parameter
        actionURL += "?action=" + request.getParameter("action") + "&performance_id=" + performanceId;
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Додати/редагувати успішність студента</title>
    <link rel="stylesheet" href="../static/css/styles.css">
</head>
<body>
    <div class="container">
        <h1>Додати/редагувати успішність студента</h1>
        <% if (errorMessage != null) { %>
            <div class="error-message"><%= errorMessage %></div>
        <% } %>

        <% if (userInputErrors != null && userInputErrors.containsKey("emptyFieldsError")) { %>
        <div class="error-message"><%= userInputErrors.get("emptyFieldsError") %></div>
        <% } %>

        <a href="${pageContext.request.contextPath}/" class="btn">Головна</a>
        <a href="PerformanceController?action=showAll" class="btn">Переглянути усіх студентів</a>

        <form action="<%= actionURL %>" method="post">
            <div>
                <input type="hidden" name="performanceId" value="${performance.performanceId}">
            </div>
            <div>
                <label for="subjectName" class="required-label">Предмет</label>
                <input type="text" id="subjectName" name="subjectName" value="${performance.subjectName}">

                <% if (userInputErrors != null && userInputErrors.containsKey("subjectNameError")) { %>
                <div class="error-message"><%= userInputErrors.get("subjectNameError") %></div>
                <% } %>
            </div>
            <div>
                <label for="lastName" class="required-label">Прізвище</label>
                <input type="text" id="lastName" name="lastName" value="${performance.lastName}">

                <% if (userInputErrors != null && userInputErrors.containsKey("lastNameError")) { %>
                <div class="error-message"><%= userInputErrors.get("lastNameError") %></div>
                <% } %>
            </div>
            <div>
                <label for="firstName" class="required-label">Ім'я</label>
                <input type="text" id="firstName" name="firstName" value="${performance.firstName}">

                <% if (userInputErrors != null && userInputErrors.containsKey("firstNameError")) { %>
                <div class="error-message"><%= userInputErrors.get("firstNameError") %></div>
                <% } %>
            </div>
            <div>
                <label for="middleName">По-батькові</label>
                <input type="text" id="middleName" name="middleName" value="${performance.middleName}">

                <% if (userInputErrors != null && userInputErrors.containsKey("middleNameError")) { %>
                <div class="error-message"><%= userInputErrors.get("middleNameError") %></div>
                <% } %>
            </div>
            <div>
                <label for="groupNo" class="required-label">Номер групи</label>
                <input type="text" id="groupNo" name="groupNo" value="${performance.groupNo}">

                <% if (userInputErrors != null && userInputErrors.containsKey("groupNoError")) { %>
                <div class="error-message"><%= userInputErrors.get("groupNoError") %></div>
                <% } %>
            </div>
            <div>
                <label for="course" class="required-label">Курс</label>
                <input type="number" id="course" name="course" value="${performance.course}">

                <% if (userInputErrors != null && userInputErrors.containsKey("courseError")) { %>
                <div class="error-message"><%= userInputErrors.get("courseError") %></div>
                <% } %>
            </div>
            <div>
                <label for="attendedLectures" class="required-label">Відвідані лекції</label>
                <input type="number" id="attendedLectures" name="attendedLectures" value="${performance.attendedLectures}">

                <% if (userInputErrors != null && userInputErrors.containsKey("attendedLecturesError")) { %>
                <div class="error-message"><%= userInputErrors.get("attendedLecturesError") %></div>
                <% } %>
            </div>
            <div>
                <label for="labWorksCompleted" class="required-label">Виконані лаб. роботи</label>
                <input type="number" id="labWorksCompleted" name="labWorksCompleted" value="${performance.labWorksCompleted}">

                <% if (userInputErrors != null && userInputErrors.containsKey("labWorksCompletedError")) { %>
                <div class="error-message"><%= userInputErrors.get("labWorksCompletedError") %></div>
                <% } %>
            </div>
            <div>
                <label for="labWorksGrades">Оцінки</label>
                <input type="text" id="labWorksGrades" name="labWorksGrades" value="${performance.labWorksGrades.replaceAll("<br>", ", ")}">

                <% if (userInputErrors != null && userInputErrors.containsKey("labWorksGradesError")) { %>
                <div class="error-message"><%= userInputErrors.get("labWorksGradesError") %></div>
                <% } %>
            </div>
            <button type="submit">Зберегти</button>
        </form>
    </div>
</body>
</html>
